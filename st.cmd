require s7plc
require essioc
require rflps_sim


############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "TS2-RFLPS2:Ctrl-IOC-001")
epicsEnvSet(IOCDIR, "TS2-RFLPS2_Ctrl-IOC-001")
epicsEnvSet(LOG_SERVER_NAME, "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=TS2-010RFC:RFS02:)")
#epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=TS2-RFLPS2:RFS-)")
epicsEnvSet("PLCIP", "$(RFLPS_IP=172.16.110.14)")

#var s7plcDebug 5

## Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "660")
epicsEnvSet("OUTSIZEAF", "390")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "372")
epicsEnvSet("OUTSIZEDIO", "62")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "390")
epicsEnvSet("OUTSIZEPSU", "222")

s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

dbLoadTemplate("$(E3_IOCSH_TOP)/subs/rflpsCPU.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(E3_IOCSH_TOP)/subs/rflpsAF.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(E3_IOCSH_TOP)/subs/rflpsDIO.substitutions", "PREFIX=$(PREFIX)")
dbLoadTemplate("$(E3_IOCSH_TOP)/subs/rflpsPSU.substitutions", "PREFIX=$(PREFIX)")

iocInit()
