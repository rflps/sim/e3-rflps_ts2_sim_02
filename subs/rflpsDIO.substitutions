global {PLC=PLCDIO}

file "rflpsConn.template"
{
	pattern {SGNL}
		{DIO}
}

file "rflpsDIOR.template"
{
	pattern {SGNL,                  SOFF,   ZNAM,   ONAM,           DESC}
			{RFS-FIM-110:HVena,	0	,"HVENA","HVOFF",        "HV Enable"}
			{FIM-110:RFena,	12	,"RFENA","RFOFF",        "RF Enable"}
			{DA-110:DAena,	24	,"DAOFF","DAENA",        "Driver Amplifier enable"}
			{CavRdy:110:Ilck,	36	,"OK",   "ITLCK",        "Cavity Ready status"}
			{Oil-110:LS,	48	,"OK",   "ITLCK",        "Oil tank level switch"}
			{Oil-110:TSH,	60	,"OK",   "ITLCK",        "Oil tank thermal-switch"}
			{Oil-110:FIS,	72	,"OK",   "ITLCK",        "Oil outlet water FS"}
			{Sol-110:FIS,	84	,"OK",   "ITLCK",        "SOL outlet water FS"}
			{Body-110:FIS,	96	,"OK",   "ITLCK",        "Body outlet water FS"}
			{Kly-110:OpenPanel,	108	,"OK",   "ITLCK",        "Klystron shielding"}
			{FIM-110:HVenaStat,	120	,"HVON","HVOFF",         "HV Enable STAT"}
			{PSS-110:Ready,	132	,"PSS-OFF","PSS-RDY",    "PSS ready"}
			{DA-110:Ready,	144	,"DAN-RDY","DA-RDY",     "Driver Amplifier ready"}
			{PSS-110:Shutter,	156	,"OK","ITLCK",         "Shutter PSS status"}
			{FIM-110:RFenaStat,	168	,"RFON","RFOFF",         "RF Enable status"}
			{ADR-110:CHA,	180	,"OK",   "ITLCK",        "AD Klystron MS"}
			{ADR-110:CHB,	192	,"OK",   "ITLCK",        "AD 110-CERN-CHB"}
			{ADR-120:CHA,	204	,"OK",   "ITLCK",        "AD 120-CERN-CHA"}
			{ADR-120:CHB,	216	,"OK",   "ITLCK",        "AD 120-CERN-CHB"}
			{Circ-110:FIS,	228	,"OK",   "ITLCK",        "CIRC water FS"}
			{TCU-110:ES,	240	,"TCU-ERR","TCU-RDY",    "TCU ready"}
			{DA-110:DArst,	252	,"DARST","DARST",        "Driver Amp. reset cmd"}
			{Kly-110:GreenLight,	264	,"STDBY-OFF","STDBY-ON", "Light indicator green"}
			{Kly-110:YellowLight,	276	,"HV-OFF","HV-ON",       "Light indicator orange"}
			{Kly-110:RedLight,	288	,"RF-OFF","RF-ON",       "Ligth indicator red"}
			{VacPS-110:HVena,	300	,"HVENA",   "HVOFF",     "Vacuum HV enable"}
			{VacPS-120:HVena,	312	,"HVENA",   "HVOFF",     "vacuum HV enable"}
			{ADR-13001:Ilck,	324	,"OK",   "ITLCK",     "AD DUT FO"}
			{ADR-13002:Ilck,	336	,"OK",   "ITLCK",     "AD Circulator FO"}
			{ADR-13003:Ilck,	348	,"OK",   "ITLCK",     "AD Isol. Load FO"}
			{ADR-13004:Ilck,	360	,"OK",   "ITLCK",     "AD Rej. Load FO"}
}

file "rflpsDIOW.template"
{
	pattern {SGNL, SOFF}
			{RFS-FIM-110:HVena,	0	}
			{FIM-110:RFena,	2	}
			{DA-110:DAena,	4	}
			{CavRdy:110:Ilck,	6	}
			{Oil-110:LS,	8	}
			{Oil-110:TSH,	10	}
			{Oil-110:FIS,	12	}
			{Sol-110:FIS,	14	}
			{Body-110:FIS,	16	}
			{Kly-110:OpenPanel,	18	}
			{FIM-110:HVenaStat,	20	}
			{PSS-110:Ready,	22	}
			{DA-110:Ready,	24	}
			{PSS-110:Shutter,	26	}
			{FIM-110:RFenaStat,	28	}
			{ADR-110:CHA,	30	}
			{ADR-110:CHB,	32	}
			{ADR-120:CHA,	34	}
			{ADR-120:CHB,	36	}
			{Circ-110:FIS,	38	}
			{TCU-110:ES,	40	}
			{DA-110:DArst,	42	}
			{Kly-110:GreenLight,	44	}
			{Kly-110:YellowLight,	46	}
			{Kly-110:RedLight,	48	}
			{VacPS-110:HVena,	50	}
			{VacPS-120:HVena,	52	}
			{ADR-13001:Ilck,	54	}
			{ADR-13002:Ilck,	56	}
			{ADR-13003:Ilck,	58	}
			{ADR-13004:Ilck,	60	}
}


file "rflpsDIOInternal.template"
{
	pattern {SGNL}
			{RFS-FIM-110:HVena}
			{FIM-110:RFena}
			{DA-110:DAena}
			{CavRdy:110:Ilck}
			{Oil-110:LS}
			{Oil-110:TSH}
			{Oil-110:FIS}
			{Sol-110:FIS}
			{Body-110:FIS}
			{Kly-110:OpenPanel}
			{FIM-110:HVenaStat}
			{PSS-110:Ready}
			{DA-110:Ready}
			{PSS-110:Shutter}
			{FIM-110:RFenaStat}
			{ADR-110:CHA}
			{ADR-110:CHB}
			{ADR-120:CHA}
			{ADR-120:CHB}
			{Circ-110:FIS}
			{TCU-110:ES}
			{DA-110:DArst}
			{Kly-110:GreenLight}
			{Kly-110:YellowLight}
			{Kly-110:RedLight}
			{VacPS-110:HVena}
			{VacPS-120:HVena}
			{ADR-13001:Ilck}
			{ADR-13002:Ilck}
			{ADR-13003:Ilck}
			{ADR-13004:Ilck}
}
